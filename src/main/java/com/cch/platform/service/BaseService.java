package com.cch.platform.service;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;


public abstract class BaseService<T> extends CommonService{
	
	private Class<T> entityClass;
	
	@SuppressWarnings("unchecked")
	@Autowired
	public void BaseService(){
		Type genType = getClass().getGenericSuperclass();
		Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
		entityClass = (Class<T>) params[0];
	}
	 
	public T get(Serializable id) {
		return beanDao.get(entityClass,id);
	}
	
	public T get(Map<String, Object> param) {
		return beanDao.get(entityClass,param);
	}
	
	public Map<String,?> pageQuery(Map<String, Object> param) {
		return beanDao.pageQuery(entityClass,param);
	}
	
	public List<T> find(Map<String, Object> params){
		return beanDao.find(entityClass,params);
	}

}
