package com.cch.stock.report.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cch.platform.dao.PageObject;
import com.cch.platform.service.BaseService;
import com.cch.platform.util.DateUtil;
import com.cch.platform.util.StringUtil;
import com.cch.platform.web.WebUtil;
import com.cch.stock.bill.bean.SdBill;

@Service
public class TradeReportService extends BaseService<SdBill> {
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map getData(Map<String, Object> param) throws Exception{
		//1、确定日期范围     from <= (?) < to
		param.put("userId", WebUtil.getUser().getUserId());
		param.put("billBigcatory","stock_trade");
		List ft=this.beanDao.findNamedQuery("SdBill.getMinDate", param);
		if(ft.get(0)==null) return null;
		Calendar dateFrom=Calendar.getInstance();
		Calendar dateTo=Calendar.getInstance();
		dateFrom.setTime((Date)(ft.get(0)));
		dateFrom.set(Calendar.HOUR_OF_DAY, 0);
		dateFrom.set(Calendar.SECOND,0);
		dateFrom.set(Calendar.MINUTE,0);
		dateFrom.set(Calendar.MILLISECOND,0);
		dateTo.set(Calendar.HOUR_OF_DAY, 0);
		dateTo.set(Calendar.SECOND,0);
		dateTo.set(Calendar.MINUTE,0);
		dateTo.set(Calendar.MILLISECOND,0);
		Calendar from=Calendar.getInstance();
		from.setTime(dateFrom.getTime());
		Calendar to=Calendar.getInstance();
		to.setTime(dateTo.getTime());
		to.add(Calendar.DAY_OF_MONTH, 1);
		
		String scope="month";
		if(param.get("scope")!=null){
			scope=(String)param.get("scope");
		}
		if ("week".equals(scope)) {
			from.set(Calendar.DAY_OF_WEEK, 1);
		} else if ("month".equals(scope)) {
			from.set(Calendar.DAY_OF_MONTH, 1);
		} else if ("quarter".equals(scope)) {
			from.set(Calendar.DAY_OF_MONTH, 1);
			int i = from.get(Calendar.MONTH) / 3 * 3;
			from.set(Calendar.MONTH, i);
		} else if ("year".equals(scope)) {
			from.set(Calendar.DAY_OF_YEAR, 1);
		}
		//求时间段
		ArrayList<Map> scopelist=new ArrayList<Map>();
		while(from.compareTo(to)<0){
			Calendar tfrom=Calendar.getInstance();
			tfrom.setTime(from.getTime());
			if ("week".equals(scope)) {
				tfrom.add(Calendar.DAY_OF_YEAR, 7);
			} else if ("month".equals(scope)) {
				tfrom.add(Calendar.MONTH, 1);
			} else if ("quarter".equals(scope)) {
				tfrom.add(Calendar.MONTH, 3);
			} else if ("year".equals(scope)) {
				tfrom.set(Calendar.YEAR, 1);
			}
//			switch (scope) {
//				case "week":
//					tfrom.add(Calendar.DAY_OF_YEAR, 7);
//					break;
//				case "month":
//					tfrom.add(Calendar.MONTH, 1);
//					break;
//				case "quarter":
//					tfrom.add(Calendar.MONTH, 3);
//					break;
//				case "year":
//					tfrom.set(Calendar.YEAR, 1);
//					break;
//				default:
//					break;
//			}
			Map scopemap=new HashMap();
			scopemap.put("dateFrom", from.compareTo(dateFrom)<0?dateFrom.getTime():from.getTime());
			scopemap.put("dateTo", tfrom.compareTo(to)<0?tfrom.getTime():to.getTime());	
			from.setTime(tfrom.getTime());
			tfrom.add(Calendar.DAY_OF_YEAR, -1);
			scopemap.put("dateTo1", tfrom.compareTo(dateTo)<0?tfrom.getTime():dateTo.getTime());
			scopemap.put("userId", WebUtil.getUser().getUserId());
			scopemap.put("billBigcatory","stock_trade");
			scopelist.add(0, scopemap);
		}
		
		int pageSize=StringUtil.parseInt(param.get("_pageSize"), 1);
		int pageNo=StringUtil.parseInt(param.get("_pageNo"), 1);
		if(pageNo<1)pageNo=1;
		PageObject page=new PageObject(pageNo, pageSize);
		page.setTotal(scopelist.size());
		List rows=new ArrayList();
		int i=page.starIndex(),j=page.starIndex()+pageSize;
		if(j>scopelist.size())j=scopelist.size();
		for(;i<j;i++){
			List<Map> tlst=this.beanDao.findNamedQuery("SdBill.report.query", scopelist.get(i));
//			if("0".equals(tlst.get(0).get("tradeTimes").toString())) continue;
			tlst.get(0).put("dateFrom", (Date)scopelist.get(i).get("dateFrom"));
			tlst.get(0).put("dateTo", (Date)scopelist.get(i).get("dateTo1"));
			rows.addAll(tlst);
		}
		page.setRows(rows);
		
		return page.getModel();
	}
	
}