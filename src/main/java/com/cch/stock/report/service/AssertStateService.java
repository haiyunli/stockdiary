package com.cch.stock.report.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cch.platform.base.bean.BaseUser;
import com.cch.platform.dao.HibernateDao;
import com.cch.stock.bill.bean.SdBill;
import com.cch.stock.bill.dao.BillDao;
import com.cch.stock.report.bean.AssertState;
import com.cch.stock.report.bean.QueryTime;
import com.cch.stock.set.bean.SdStockDaliy;
import com.cch.stock.set.dao.AssertInitDao;
import com.cch.stock.set.service.StockPriceService;

@Service
public class AssertStateService {
	
	@Autowired
	AssertInitDao astInit;
	
	@Autowired
	BillDao billDao;
	
	@Autowired
	StockPriceService spService;

	@SuppressWarnings("unchecked")
	public Map<String,List<Object>> getAssertState(BaseUser user,QueryTime qt){
		//处理querytime
		setDefaultTime(user, qt);
		
		AssertState ast=this.getInitAssertState(user, qt.getBeginDay());
		Map<String,List<Object>> re=ast.getDataArray();
		
		String hql="from SdBill where userId=? and billDate>=? and billDate<=? order by billDate asc";
		List<SdBill> tmpList = billDao.find(hql, user.getUserId(),qt.getBeginDay(),qt.getEndDay());
		Date cmpDate;
		int i=0;
		while((cmpDate=qt.getNextDay())!=null){
			while(i<tmpList.size()&&tmpList.get(i).getBillDate().compareTo(cmpDate)<0){
				ast.addBill(tmpList.get(i));
				i++;
			}
			this.updateStockValue(ast, cmpDate);
			ast.getDataArray(re);
		}
		return re;
	}
	
	public AssertState getInitAssertState(BaseUser user,Date beginTime){
		AssertState ast=new AssertState();
		ast.setUser(user);
		ast.setBeginTime(beginTime);
		
		String hqla="select sum(billMoney) from SdBill where userId=? and billDate<?";
		List tmpList = billDao.find(hqla, user.getUserId(),beginTime);
		if(tmpList.get(0)==null){
			ast.setMoney(0);
		}else{
			ast.setMoney( (Double) (tmpList.get(0)) );
		}
		
		String hqlb="select new Map(stockCode as stockCode,sum(stockCnt) as stockCnt ) "
				+ " from SdBill where userId=? and billDate<? group by stockCode having sum(stockCnt)!=0";
		Map<String,Integer> stocks=new HashMap<String,Integer>();
		tmpList = billDao.find(hqlb, user.getUserId(),beginTime);
		for(int i=0;i<tmpList.size();i++){
			Map tmap=(Map) tmpList.get(i);
			stocks.put(tmap.get("stockCode").toString(), ((Long)tmap.get("stockCnt")).intValue());
		}
		ast.setStocks(stocks);
		
		this.updateStockValue(ast,beginTime);
		ast.setAllCost(ast.getAllAssert());
		ast.setProfit(0);
		return ast;
	}
	
	private void updateStockValue(AssertState ast,Date nowTime) {
		ast.setNowTime(nowTime);
		double stockValue=0;
		Map<String,Integer> stocks=ast.getStocks();
		for(Entry<String, Integer> stock:stocks.entrySet()){
			SdStockDaliy stockPrice=spService.getPrice(stock.getKey(),nowTime);
			if(stockPrice!=null)
				stockValue+=stock.getValue()*stockPrice.getPriceClose();
		}
		ast.updateStockValue(stockValue);
	}
	
	private void setDefaultTime(BaseUser user,QueryTime qt){
		Date beginDay=astInit.getInitDate(user); 
		if(beginDay==null){
			beginDay=billDao.getFirstBillDate(user);
		}
		if(beginDay==null){
			beginDay=new Date();
		}
		if(null==qt.getBeginDay()){
			qt.setBeginDay(beginDay);
		} else if(beginDay.compareTo(qt.getBeginDay())>0){
			qt.setBeginDay(beginDay);
		}
		if(null==qt.getEndDay()){
			qt.setEndDay(new Date());
		}
		if(null==qt.getPeriod()){
			qt.setPeriod(QueryTime.Period.week);
		}	
	}
}
