/**
 * 
 */
(function($) {
	//改写 ajax函数，增加mask遮罩属性
	$._ajax_=$.ajax;
	$.ajax=function( url, options ) {
		var _op=options;
		if ( typeof url === "object" ) {
			_op = url;
		}
		if(_op.mask){
			$.messager.progress();
			_op.success=function(){
				$.messager.progress("close");
			}
		}
		return $._ajax_( url, options );
	}
	
	$.ajaxSetup({
		timeout : 6000000,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert(XMLHttpRequest+textStatus+errorThrown);
		},
		dataFilter : function(data, type) {
			var jdata=data;
			if($.type(data) === "string"&&data.length<500&&
					(data.indexOf("message")>-1||data.indexOf("location")>-1) ){
				jdata=eval("("+data+")");
	    		if(jdata&&jdata.sucesse){
		    		if(jdata.sucesse=="false"||jdata.sucesse==false){
		    			if(jdata.message){
		    				$.messager.alert('错误',jdata.message,'error');
		    			} else if(jdata.location&&jdata.location.indexOf("login/login.jsp")>-1){
		    				var _win=plat.WinUtil.top();
		    				_win.location.pathname=jdata.location;
		    			} else{
		    				$.messager.alert('错误','发生错误!','error');
		    			}	  
		    			return false;
	      			}else{
		    			if(jdata.message){
		    				$.messager.alert('提示',jdata.message,'info');
		    			} 
	      			}
	    		}
			}
			return data;
		}
	});
	
	$.extend($.fn, {
		serializeJson: function(field){
	        var formArray = this.serializeArray();
	        var oRet = {};
	        for (var i in formArray) {
	            if (typeof(oRet[formArray[i].name]) == 'undefined') 
	            	oRet[formArray[i].name] = formArray[i].value;
	            else  oRet[formArray[i].name] += "," + formArray[i].value;
	        }
	        if(field) return oRet[field];
	        return oRet;
	    }
	});

})(jQuery);
